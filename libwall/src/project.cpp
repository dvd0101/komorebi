#include "wall/project.h"

namespace wall {
	project::project(size r) : resolution_{r} {}

	void project::set_resolution(size r) {
		resolution_ = r;
	}

	size project::resolution() const {
		return resolution_;
	}
}
