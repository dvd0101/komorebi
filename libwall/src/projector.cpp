#include "wall/projector.h"

namespace wall {
	projector_data::projector_data(std::string n, size r) : name_{std::move(n)}, resolution_{r} {
		if (resolution_.width == 0 || resolution_.height == 0) {
			throw projector_error("invalid resolution");
		}
	}

	const std::string& projector_data::name() const {
		return name_;
	}

	size projector_data::resolution() const {
		return resolution_;
	}

	projector::projector(std::string n, size r) : projector_data{n, r}, origin_{0, 0} {}

	void projector::origin(point p) {
		origin_ = p;
	}

	point projector::origin() const {
		return origin_;
	}
}
