#include "catch.hpp"
#include "wall/parser.h"

namespace {
	struct test_handler {
		void command(const wall::command& cmd) {
			commands.push_back(cmd);
		}

		void parse_failure(std::string_view data) {
			failures.emplace_back(data);
		}

		void done() {}

		std::vector<wall::command> commands;
		std::vector<std::string> failures;
	};
}

using wall::extract_commands;

SCENARIO("The extract_commands is called over an empty range", "[input]") {
	GIVEN("an empty vector") {
		std::vector<char> buffer;
		test_handler handler;

		WHEN("the extract_commands is called over the range") {
			auto pos = extract_commands(begin(buffer), end(buffer), handler);

			THEN("no commands are extracted and the return value is equal to both the begin and the end of the range") {
				REQUIRE(handler.commands.size() == 0);
				REQUIRE(pos == begin(buffer));
				REQUIRE(pos == end(buffer));
			}
		}
	}
}

SCENARIO("The extract_commands is called over a range containing some data", "[input]") {
	GIVEN("a string with two complete commands and one incomplete") {
		std::string buffer = "NOOP\nPAUSE 5\nPAU";
		test_handler handler;

		WHEN("extract_commands is called over the entire range") {
			auto pos = extract_commands(begin(buffer), end(buffer), handler);

			THEN("the two complete commands are extracted") {
				REQUIRE(handler.commands.size() == 2);
				std::get<wall::commands::noop>(handler.commands[0]);
				std::get<wall::commands::pause>(handler.commands[1]);
			}

			AND_THEN("the incomplete command has been copied to the start of the range") {
				REQUIRE(buffer.find("PAU") == 0);
			}

			AND_THEN("the return value points after the copied command") {
				REQUIRE(pos == begin(buffer) + 3);
			}
		}

		WHEN("extract_commands is called over the portion that contains the two complete commands") {
			auto pos = extract_commands(begin(buffer), std::next(begin(buffer), 13), handler);

			THEN("only the selected portion is analyzed, and the two commands added to the handler") {
				REQUIRE(handler.commands.size() == 2);
				std::get<wall::commands::noop>(handler.commands[0]);
				std::get<wall::commands::pause>(handler.commands[1]);
				REQUIRE(pos == begin(buffer));
			}
		}

		WHEN("extract_commands is called over a range that doesn not ends with a new line") {
			auto pos = extract_commands(begin(buffer), std::next(begin(buffer), 4), handler);

			THEN("even if the range contains a valid command the data is not parsed") {
				REQUIRE(handler.commands.size() == 0);
				REQUIRE(pos == std::next(begin(buffer), 4));
			}
		}

		WHEN("extract_commands is called over a range that contains an unknown command") {
			auto pos = extract_commands(begin(buffer) + 1, begin(buffer) + 5, handler);

			THEN("the range is parsed and the unrecognized data is added to the handler") {
				REQUIRE(handler.commands.size() == 0);
				REQUIRE(handler.failures.size() == 1);
				REQUIRE(handler.failures[0] == "OOP");
				REQUIRE(pos == begin(buffer) + 1);
			}
		}
	}
}
