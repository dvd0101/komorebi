#include "wall/state.h"
#include <ostream>
#include <sstream>
#include <experimental/iterator>

namespace wall {

	std::ostream& operator<<(std::ostream& out, grid_cell cell) {
		return out  << "["
					<< cell.top_left << ","
					<< cell.bottom_left << ","
					<< cell.bottom_right << ","
					<< cell.top_right
					<< "]";
	}

	std::ostream& operator<<(std::ostream& out, grid_shape s) {
		return out << "{\"cols\": " << +s.columns << ", \"rows\": " << +s.rows << "}";
	}

	static std::ostream& operator<<(std::ostream& out, const std::vector<screen_pos>& row) {
		out << "[";
		std::copy(row.begin(), row.end(), std::experimental::make_ostream_joiner(out, ","));
		out << "]";
		return out;
	}

	std::ostream& operator<<(std::ostream& out, const grid& grid) {
		out << "{";
		out << "\"size\": " << grid.size() << ",";
		out << "\"vertices\": [";
		std::copy(
			grid.rows_begin(),
			grid.rows_end(),
			std::experimental::make_ostream_joiner(out, ","));
		out << "]";
		out << "}";
		return out;
	}

	std::ostream& operator<<(std::ostream& out, const project& p) {
		return out << "{\"resolution\": " << p.resolution() << "}";
	}

	std::ostream& operator<<(std::ostream& out, const projector& p) {
		return out  << "{"
					<< "\"name\": \"" << p.name() << "\","
					<< "\"resolution\": " << p.resolution() << ","
					<< "\"origin\": " << p.origin()
					<< "}";
	}

	std::ostream& operator<<(std::ostream& out, const state& s) {
		return out  << "{"
					<< "\"project\": " << s.project() << ","
					<< "\"projectors\": ["
					<< "{"
						<< "\"metadata\": " << s.local_projector() << ","
						<< "\"grid\": " << s.grid()
					<< "}"
					<< "]"
					<< "}";
	}

	std::string to_string(const state& state) {
		std::ostringstream s;
		s << state;
		return s.str();
	}
}
