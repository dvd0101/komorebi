#include "wall/commands.h"
#include <sstream>

namespace {
	using namespace wall::commands;

	template <typename ...Args>
	std::string format(Args&&... args) {
		std::ostringstream s;
		auto unused = {(s << args, 0)...};
		(void)unused;
		return s.str();
	}

	struct visitor {
		std::string operator()(const add_row& cmd) const {
			return format("ADD ROW ", +cmd.row);
		}

		std::string operator()(const add_col& cmd) const {
			return format("ADD COL ", +cmd.col);
		}

		std::string operator()(const dump_state&) const {
			return format("DUMP");
		}

		std::string operator()(const load_image& cmd) const {
			return format("LOAD IMAGE ", cmd.file_path);
		}

		std::string operator()(const noop&) const {
			return format("NOOP");
		}

		std::string operator()(const pause& cmd) const {
			return format("PAUSE ", cmd.ms);
		}

		std::string operator()(const set_grid& cmd) const {
			return format("GRID ", +cmd.cols(), ",", +cmd.rows());
		}

		std::string operator()(const set_node& cmd) const {
			return format(
				"SET NODE ",
				+cmd.node.col, ",", +cmd.node.row, ' ',
				cmd.screen.col, ",", cmd.screen.row);
		}

		std::string operator()(const set_project& cmd) const {
			return format("PROJECT ", +cmd.width(), ",", +cmd.height());
		}

		std::string operator()(const set_projector& cmd) const {
			std::string t = "SET PROJECTOR ";
			if (cmd.projector_name != "") {
				t += cmd.projector_name + " ";
			}
			return t + format(cmd.x, ",", cmd.y);
		}
	};
}

namespace wall {
	namespace commands {
		grid_error::grid_error() : command_error{"grid size out of range"} {}
		project_error::project_error() : command_error{"project size out of range"} {}

		set_grid::set_grid(uint8_t c, uint8_t r) : cols_{c}, rows_{r} {
			if (cols_ == 0 || rows_ == 0) {
				throw grid_error();
			}
		}

		uint8_t set_grid::rows() const {
			return rows_;
		};

		uint8_t set_grid::cols() const {
			return cols_;
		};

		set_project::set_project(uint16_t width, uint16_t height) : width_{width}, height_{height} {
			if (width == 0 || height == 0) {
				throw project_error();
			}
		}

		uint16_t set_project::width() const {
			return width_;
		}

		uint16_t set_project::height() const {
			return height_;
		}
	}

	std::string to_string(const command& cmd) {
		return std::visit(visitor(), cmd);
	}
}
