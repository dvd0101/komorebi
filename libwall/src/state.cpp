#include "wall/state.h"
#include "overloaded.h"
#include <iostream>

namespace wall {
	state::state(const projector_data& p)
	    : project_{size{1920, 1080}}, local_projector_{p.name(), p.resolution()}, grid_{{3, 3}} {
		set(changes::project);
		set(changes::grid);
		set(changes::projector);
	}

	const wall::project& state::project() const {
		return project_;
	}

	const wall::projector& state::local_projector() const {
		return local_projector_;
	}

	const wall::grid& state::grid() const {
		return grid_;
	}

	const wall::image* state::image() const {
		return image_ ? &(*image_) : nullptr;
	}

	void state::process(const commands::add_col&) {}
	void state::process(const commands::add_row&) {}

	void state::process(const commands::set_project& cmd) {
		project_.set_resolution({cmd.width(), cmd.height()});
		set(changes::project);
	}

	void state::process(const commands::set_projector& cmd) {
		if (cmd.projector_name == "" || cmd.projector_name == local_projector_.name()) {
			local_projector_.origin({cmd.x, cmd.y});
			set(changes::projector);
		}
	}

	void state::process(const commands::load_image& cmd) {
		wall::image img(cmd.file_path);
		img.load();
		image_ = std::move(img);
		set(changes::image);
	}

	void state::process(const commands::set_grid& cmd) {
		grid_ = wall::grid({cmd.cols(), cmd.rows()});
		set(changes::grid);
	}

	void state::process(const commands::set_node& cmd) {
		grid_.set_node(cmd.node, cmd.screen);
		set(changes::grid);
	}

	namespace {
		template <typename T>
		std::optional<std::string> execute(const T& cmd, state& s, commands::state_change_t) {
			s.process(cmd);
			return std::nullopt;
		}

		template <typename T>
		std::optional<std::string> execute(const T&, state&, commands::state_query_t) {
			return std::nullopt;
		}

		std::optional<std::string> execute(const commands::dump_state&, state& s, commands::state_query_t) {
			return to_string(s);
		}
	}

	std::optional<std::string> execute(const command& cmd, state& s) {
		return std::visit(
			[&](const auto& c) {
				using command_t = std::remove_cv_t<std::remove_reference_t<decltype(c)>>;
				using tag = typename command_t::tag;
				return execute(c, s, tag{});
			},
			cmd);
	}
}

#ifdef WALL_TEST
#include "catch.hpp"

using namespace wall;

auto test_state = []() {
	projector_data test_projector("local", {10, 10});
	state s(test_projector);
	s.process(commands::set_project(20, 20));
	return s;
};

SCENARIO("The local projector is moved outside the project area", "[state]") {
	GIVEN("A state instance") {
		auto s = test_state();

		WHEN("the local projector is positioned near the end of the project area") {
			s.process(commands::set_projector{{}, "", 15, 0});

			THEN("the final position is changed even if the projection area is partially outside of the project") {
				REQUIRE(s.local_projector().origin() == point{15, 0});
			}
		}

		WHEN("the local projector is positioned completely outside of the project area") {
			s.process(commands::set_projector{{}, "", 25, 0});

			THEN("the final position is changed even if the projection area is outside of the project") {
				REQUIRE(s.local_projector().origin() == point{25, 0});
			}
		}

		WHEN("the local projector is positioned completely at the left of the project area") {
			s.process(commands::set_projector{{}, "", -25, 0});

			THEN("the final position is changed even if the projection area is outside of the project") {
				REQUIRE(s.local_projector().origin() == point{-25, 0});
			}
		}
	}
}
#endif
