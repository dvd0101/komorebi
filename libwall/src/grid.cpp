#include "wall/grid.h"
#include <gsl/gsl>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconversion"
#include <range/v3/all.hpp>
#pragma GCC diagnostic pop
#include <tuple>

using namespace ranges;

namespace wall {
	bool grid_cell::operator==(grid_cell other) const {
		return top_left == other.top_left && bottom_left == other.bottom_left && bottom_right == other.bottom_right
		    && top_right == other.top_right;
	}

	bool grid_shape::operator==(grid_shape other) const {
		return columns == other.columns && rows == other.rows;
	}

	grid::grid(grid_shape s) : cols_{s.columns}, rows_{s.rows} {
		if (cols_ == 0 || rows_ == 0) {
			throw invalid_shape();
		}
		reset();
	}

	grid_shape grid::size() const {
		return {cols_, rows_};
	}

	grid_cell grid::cell(col c, row r) const {
		const auto pc = static_cast<uint8_t>(c);
		const auto pr = static_cast<uint8_t>(r);
		if (pc >= cols_ || pr >= rows_) {
			throw std::out_of_range("cell position out of range");
		}
		return {
		    vertices.get(pc, pr), vertices.get(pc, pr + 1), vertices.get(pc + 1, pr + 1), vertices.get(pc + 1, pr),
		};
	}

	grid_cell grid::original_cell(col c, row r) const {
		const auto pc = static_cast<uint8_t>(c);
		const auto pr = static_cast<uint8_t>(r);
		if (pc >= cols_ || pr >= rows_) {
			throw std::out_of_range("cell position out of range");
		}
		return {
		    screen_pos{original_columns_[pc], original_rows_[pr]},         //
		    screen_pos{original_columns_[pc], original_rows_[pr + 1]},     //
		    screen_pos{original_columns_[pc + 1], original_rows_[pr + 1]}, //
		    screen_pos{original_columns_[pc + 1], original_rows_[pr]},     //
		};                                                                 //
	}

	grid::row_iterator grid::rows_begin() const {
		return vertices.begin();
	}

	grid::row_iterator grid::rows_end() const {
		return vertices.end();
	}

	void grid::reset() {
		const float col_step = screen_pos::range / static_cast<float>(cols_);
		const float row_step = screen_pos::range / static_cast<float>(rows_);
		vertices.resize(cols_ + 1, rows_ + 1, [=](size_t col, size_t row) {
			return screen_pos{gsl::narrow_cast<int16_t>(screen_pos::min + col_step * static_cast<float>(col)),
			                  gsl::narrow_cast<int16_t>(screen_pos::max - row_step * static_cast<float>(row))};
		});
		original_rows_ = view::indices(0, rows_ + 1) | view::transform([row_step](int row) {
			                 return gsl::narrow_cast<int16_t>(screen_pos::max - row_step * static_cast<float>(row));
			             });
		original_columns_ = view::indices(0, cols_ + 1) | view::transform([col_step](int col) {
			                    return gsl::narrow_cast<int16_t>(screen_pos::min + col_step * static_cast<float>(col));
			                });
	}

	void grid::add_col(col c) {
		auto prev_node = static_cast<uint8_t>(c);
		if (prev_node >= cols_) {
			throw invalid_node("the selected column is out of range");
		}
		if (cols_ + 1 > node_pos::max) {
			throw invalid_shape();
		}

		const auto row_step = screen_pos::range / rows_;
		const auto col_size = (vertices.get(prev_node + 1, 0).col - vertices.get(prev_node, 0).col);

		screen_pos new_col;
		new_col.col = gsl::narrow_cast<int16_t>(col_size / 2);
		vertices.add_col(prev_node, [=](size_t r) mutable {
			new_col.row = gsl::narrow_cast<int16_t>(screen_pos::max - row_step * r);
			return new_col;
		});

		original_columns_.push_back(new_col.col);
		std::rotate(original_columns_.rbegin(),
		            original_columns_.rbegin() + 1,
		            original_columns_.rend() - static_cast<uint8_t>(c) - 1);

		++cols_;
	}

	void grid::add_row(row r) {
		auto prev_node = static_cast<uint8_t>(r);
		if (prev_node >= rows_) {
			throw invalid_node("the selected row is out of range");
		}
		if (rows_ + 1 > node_pos::max) {
			throw invalid_shape();
		}

		const auto col_step = screen_pos::range / cols_;
		const auto row_size = (vertices.get(0, prev_node + 1).row - vertices.get(0, prev_node).row);

		screen_pos new_row;
		new_row.row = gsl::narrow_cast<int16_t>(row_size / 2);
		vertices.add_row(prev_node, [=](size_t c) mutable {
			new_row.col = gsl::narrow_cast<int16_t>(screen_pos::min + col_step * c);
			return new_row;
		});

		original_rows_.push_back(new_row.row);
		std::rotate(
		    original_rows_.rbegin(), original_rows_.rbegin() + 1, original_rows_.rend() - static_cast<uint8_t>(r) - 1);
		++rows_;
	}

	screen_pos grid::node(node_pos node) const {
		if (node.col > cols_ || node.row > rows_) {
			throw invalid_node("invalid node");
		}
		return vertices.get(node.col, node.row);
	}

	void grid::set_node(node_pos node, screen_pos pos) {
		if (node.col > cols_ || node.row > rows_) {
			throw invalid_node("invalid node");
		}
		node_pos top_left{node.col == 0 ? node.col : gsl::narrow_cast<uint8_t>(node.col - 1),
		                  node.row == 0 ? node.row : gsl::narrow_cast<uint8_t>(node.row - 1)};
		node_pos bottom_right{node.col == cols_ ? node.col : gsl::narrow_cast<uint8_t>(node.col + 1),
		                      node.row == rows_ ? node.row : gsl::narrow_cast<uint8_t>(node.row + 1)};
		{
			const auto sentinel = this->node(top_left);
			if ((top_left.col != node.col && pos.col < sentinel.col)
			    || (top_left.row != node.row && pos.row > sentinel.row)) {
				throw invalid_shape();
			}
		}
		{
			const auto sentinel = this->node(bottom_right);
			if ((bottom_right.col != node.col && pos.col > sentinel.col)
			    || (bottom_right.row != node.row && pos.row < sentinel.row)) {
				throw invalid_shape();
			}
		}
		vertices.set(node.col, node.row, pos);
	}

	std::vector<grid_cell> cells(const grid& g) {
		const auto shape = g.size();
		std::vector<grid_cell> output = view::cartesian_product(view::indices(shape.rows), view::indices(shape.columns))
		    | view::transform([&g](auto coords) { return g.cell(col(std::get<1>(coords)), row(std::get<0>(coords))); });
		return output;
	}

	std::vector<grid_cell> original_cells(const grid& g) {
		const auto shape = g.size();
		std::vector<grid_cell> output = view::cartesian_product(view::indices(shape.rows), view::indices(shape.columns))
		    | view::transform([&g](auto coords) {
				return g.original_cell(col(std::get<1>(coords)), row(std::get<0>(coords)));
			});
		return output;
	}
}

#ifdef WALL_TEST
#include "catch.hpp"

using namespace wall;

grid_cell square_cell(screen_pos top_left, screen_pos bottom_right) {
	return grid_cell{top_left, {top_left.col, bottom_right.row}, bottom_right, {bottom_right.col, top_left.row}};
}

SCENARIO("An invalid grid is constructed", "[state]") {
	WHEN("a grid with zero columns is constructed") {
		THEN("an exception is raised") {
			REQUIRE_THROWS_AS(grid({0, 1}), invalid_shape);
		}
	}
	WHEN("a grid with zero rows is constructed") {
		THEN("an exception is raised") {
			REQUIRE_THROWS_AS(grid({1, 0}), invalid_shape);
		}
	}
}

SCENARIO("A valid grid is constructed and its cells retrieved", "[state]") {
	GIVEN("a 2x2 grid") {
		grid g({2, 2});

		THEN("the grid nodes are evenly spaced across the screen space") {
			REQUIRE(g.cell(col(0), row(0)) == square_cell({-32767, 32767}, {0, 0}));
			REQUIRE(g.cell(col(1), row(0)) == square_cell({0, 32767}, {32767, 0}));
			REQUIRE(g.cell(col(0), row(1)) == square_cell({-32767, 0}, {0, -32767}));
			REQUIRE(g.cell(col(1), row(1)) == square_cell({0, 0}, {32767, -32767}));
		}

		WHEN("a new column is added over the column #1") {
			g.add_col(col{1});
			REQUIRE(g.size() == grid_shape{3, 2});

			THEN("the cells of the column #0 do not change") {
				REQUIRE(g.cell(col(0), row(0)) == square_cell({-32767, 32767}, {0, 0}));
				REQUIRE(g.cell(col(0), row(1)) == square_cell({-32767, 0}, {0, -32767}));
			}
			AND_THEN("the cells of the columns #1 and #2 share the space previously allocated for the column #1") {
				REQUIRE(g.cell(col(1), row(0)) == square_cell({0, 32767}, {16383, 0}));
				REQUIRE(g.cell(col(2), row(0)) == square_cell({16383, 32767}, {32767, 0}));
				REQUIRE(g.cell(col(1), row(1)) == square_cell({0, 0}, {16383, -32767}));
				REQUIRE(g.cell(col(2), row(1)) == square_cell({16383, 0}, {32767, -32767}));
			}
		}

		WHEN("a new row is added over the row #1") {
			g.add_row(row{1});
			REQUIRE(g.size() == grid_shape{2, 3});

			THEN("the cells of the row #0 do not change") {
				REQUIRE(g.cell(col(0), row(0)) == square_cell({-32767, 32767}, {0, 0}));
				REQUIRE(g.cell(col(1), row(0)) == square_cell({0, 32767}, {32767, 0}));
			}
			AND_THEN("the cells of the rows #1 and #2 share the space previously allocated for the row #1") {
				REQUIRE(g.cell(col(0), row(1)) == square_cell({-32767, 0}, {0, -16383}));
				REQUIRE(g.cell(col(1), row(1)) == square_cell({0, 0}, {32767, -16383}));
				REQUIRE(g.cell(col(0), row(2)) == square_cell({-32767, -16383}, {0, -32767}));
				REQUIRE(g.cell(col(1), row(2)) == square_cell({0, -16383}, {32767, -32767}));
			}
		}
	}
}

SCENARIO("A grid node is modified", "[state]") {
	GIVEN("a 4x4 grid") {
		grid g({4, 4});

		THEN("the screen position of a node can be moved around it's original position") {
			REQUIRE(g.node({2, 2}) == screen_pos{0, 0});
			g.set_node({2, 2}, {-10, -10});
			REQUIRE(g.node({2, 2}) == screen_pos{-10, -10});
			g.set_node({2, 2}, {-10, 10});
			REQUIRE(g.node({2, 2}) == screen_pos{-10, 10});
			g.set_node({2, 2}, {10, 10});
			REQUIRE(g.node({2, 2}) == screen_pos{10, 10});
			g.set_node({2, 2}, {10, -10});
			REQUIRE(g.node({2, 2}) == screen_pos{10, -10});
		}

		WHEN("the updated position of a node intersects a neighbours") {
			THEN("an exception is raised") {
				REQUIRE_THROWS_AS(g.set_node({2, 2}, {-17000, 0}), invalid_shape);
				REQUIRE_THROWS_AS(g.set_node({2, 2}, {0, 17000}), invalid_shape);
				REQUIRE_THROWS_AS(g.set_node({2, 2}, {17000, 0}), invalid_shape);
				REQUIRE_THROWS_AS(g.set_node({2, 2}, {0, -17000}), invalid_shape);
			}
		}

		WHEN("a border node is moved in a valid position") {
			g.set_node({0, 0}, {-30000, 30000});
			THEN("it can be moved back to its original position (even if now that position is outside of the grid "
			     "perimeter)") {
				g.set_node({0, 0}, {-32767, 32767});
				REQUIRE(g.node({0, 0}) == screen_pos{-32767, 32767});
			}
		}
	}
}
#endif
