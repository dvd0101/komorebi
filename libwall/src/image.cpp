#include "wall/image.h"
#include <experimental/filesystem>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic ignored "-Wimplicit-fallthrough"
#define STB_IMAGE_IMPLEMENTATION
#define STBI_FAILURE_USERMSG
#include "stb_image.h"
#pragma GCC diagnostic pop

namespace wall {
	image::image(std::string p) : path_{std::move(p)}, data_{nullptr, image::release}, width_{0}, height_{0} {
		if (!std::experimental::filesystem::exists(path_)) {
			throw image_not_found();
		}
	}

	gsl::span<const uint8_t> image::load() {
		int ch;
		uint8_t* ptr = stbi_load(path_.c_str(), &width_, &height_, &ch, 3);
		if (ptr == nullptr) {
			throw image_load_failed(stbi_failure_reason());
		}
		data_.reset(ptr);
		return data();
	}

	gsl::span<const uint8_t> image::data() const {
		if (data_) {
			return {data_.get(), width_ * height_ * 3};
		}
		return {};
	}

	int image::width() const {
		return width_;
	}

	int image::height() const {
		return height_;
	}

	void image::release(uint8_t* ptr) {
		stbi_image_free(ptr);
	}
}
