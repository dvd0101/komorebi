#include "wall/parser.h"
#include "overloaded.h"
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wconversion"
#include <boost/fusion/adapted/struct/adapt_struct.hpp>
#include <boost/fusion/include/at_c.hpp>
// #define BOOST_SPIRIT_X3_DEBUG
#include <boost/spirit/home/x3.hpp>
#include <boost/variant/apply_visitor.hpp>
#pragma GCC diagnostic pop

BOOST_FUSION_ADAPT_STRUCT(wall::node_pos, col, row)
BOOST_FUSION_ADAPT_STRUCT(wall::screen_pos, col, row)
BOOST_FUSION_ADAPT_STRUCT(wall::commands::pause, ms)
BOOST_FUSION_ADAPT_STRUCT(wall::commands::set_node, node, screen)
BOOST_FUSION_ADAPT_STRUCT(wall::commands::set_projector, projector_name, x, y)
BOOST_FUSION_ADAPT_STRUCT(wall::commands::add_row, row)
BOOST_FUSION_ADAPT_STRUCT(wall::commands::add_col, col)
BOOST_FUSION_ADAPT_STRUCT(wall::commands::load_image, file_path)

namespace x3 = boost::spirit::x3;

namespace {
	namespace parsers {
		x3::rule<class node_pos_, wall::node_pos> node_pos = "NODE_POS";
		const auto node_pos_def = x3::uint8 >> ',' >> x3::uint8;

		x3::rule<class screen_pos_, wall::screen_pos> screen_pos = "SCREEN_POS";
		const auto screen_pos_def = x3::int16 >> ',' >> x3::int16;

		namespace c = wall::commands;

		x3::rule<class add_row_, c::add_row> add_row = "ADD_ROW";
		const auto add_row_def = "ADD ROW" >> x3::uint8;

		x3::rule<class add_row_, c::add_col> add_col = "ADD_COL";
		const auto add_col_def = "ADD COL" >> x3::uint8;

		x3::rule<class dump_state_, c::dump_state> dump_state = "DUMP";
		const auto dump_state_def = x3::lit("DUMP")[([](auto& ctx) { x3::_val(ctx) = c::dump_state{}; })];

		x3::rule<class load_image_, c::load_image> load_image = "LOAD_IMAGE";
		const auto load_image_def = "LOAD IMAGE" >> +x3::char_;

		x3::rule<class noop_, c::noop> noop = "NOOP";
		const auto noop_def = x3::lit("NOOP") >> x3::attr(c::noop{});

		x3::rule<class pause_, c::pause> pause = "PAUSE";
		const auto pause_def = "PAUSE" >> x3::uint_;

		const auto set_grid_adapt = [](auto& ctx) {
			using boost::fusion::at_c;
			auto& attr = x3::_attr(ctx);
			x3::_val(ctx) = c::set_grid(at_c<0>(attr), at_c<1>(attr));
		};
		x3::rule<class set_grid_, std::optional<c::set_grid>> set_grid = "SET_GRID";
		const auto set_grid_def = ("SET GRID" >> x3::uint8 >> ',' >> x3::uint8)[set_grid_adapt];

		x3::rule<class set_node_, c::set_node> set_node = "SET_NODE";
		const auto set_node_def = "SET NODE" >> x3::lexeme[node_pos] >> x3::lexeme[screen_pos];

		const auto set_project_adapt = [](auto& ctx) {
			using boost::fusion::at_c;
			auto& attr = x3::_attr(ctx);
			x3::_val(ctx) = c::set_project(at_c<0>(attr), at_c<1>(attr));
		};
		x3::rule<class set_project_, std::optional<c::set_project>> set_project = "SET_PROJECT";
		const auto set_project_def = ("SET PROJECT" >> x3::uint16 >> ',' >> x3::uint16)[set_project_adapt];

		x3::rule<class set_projector_, c::set_projector> set_projector = "SET_PROJECTOR";
		const auto set_projector_def
		    = "SET PROJECTOR" >> -('[' >> +(x3::char_ - ']') >> ']') >> x3::int32 >> ',' >> x3::int32;

		const auto command_adapt = [](auto& ctx) {
			// clang-format disable
			wall::command cmd;
			boost::apply_visitor(
			    wall::overloaded{
			        [&](std::optional<wall::commands::set_grid>& v) { cmd = *v; },
			        [&](std::optional<wall::commands::set_project>& v) { cmd = *v; },
			        [&](auto& v) { cmd = std::move(v); },
			    },
			    x3::_attr(ctx));
			x3::_val(ctx) = cmd;
			// clang-format enable
		};
		x3::rule<class command_, wall::command> command = "COMMAND";
		const auto command_def = (           //
		    add_col                          //
		    | add_row                        //
		    | dump_state                     //
		    | load_image                     //
		    | noop                           //
		    | pause                          //
		    | set_grid                       //
		    | set_node                       //
		    | set_project                    //
		    | set_projector)[command_adapt]; //

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
		BOOST_SPIRIT_DEFINE( //
		    add_col,         //
		    add_row,         //
		    command,         //
		    dump_state,      //
		    load_image,      //
		    node_pos,        //
		    noop,            //
		    pause,           //
		    screen_pos,      //
		    set_grid,        //
		    set_node,        //
		    set_project,     //
		    set_projector);  //
#pragma GCC diagnostic pop
	}
}

namespace wall {
	std::optional<command> parse(std::string_view input) {
		auto first = begin(input);
		auto last = end(input);
		wall::command cmd;
		bool r = x3::phrase_parse(first, last, parsers::command, x3::ascii::space, cmd);
		if (r) {
			return cmd;
		}
		return std::nullopt;
	}
}

#ifdef WALL_TEST
#include "catch.hpp"

using namespace wall;

TEST_CASE("commands parsers", "[input]") {
	SECTION("command SET NODE") {
		auto cmd = std::get<commands::set_node>(*parse("SET NODE 1,2 1000,2000"));
		REQUIRE(cmd.node == node_pos{1, 2});
		REQUIRE(cmd.screen == screen_pos{1000, 2000});
	}
	SECTION("command SET NODE with negative node coordinates") {
		auto cmd = parse("SET NODE -1,-2 1000,2000");
		REQUIRE(!cmd);
	}
	SECTION("command SET NODE with negative screen coordinates") {
		auto cmd = std::get<commands::set_node>(*parse("SET NODE 1,2 -1000,-2000"));
		REQUIRE(cmd.node == node_pos{1, 2});
		REQUIRE(cmd.screen == screen_pos{-1000, -2000});
	}
}
#endif
