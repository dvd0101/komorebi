#pragma once

namespace wall {
	template <class... Ts>
	struct overloaded : Ts...
	// copied from:
	// http://en.cppreference.com/w/cpp/utility/variant/visit
	{
		using Ts::operator()...;
	};

	template <class... Ts>
	overloaded(Ts...)->overloaded<Ts...>;
}
