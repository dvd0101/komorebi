#include "catch.hpp"
#include "wall/coordinates.h"
#include "wall/grid_range.h"

using namespace wall;
using namespace ranges;

struct ndc_pos {
	float x;
	float y;

	bool operator==(ndc_pos other) const {
		const float eps = 0.00005f;
		return std::abs(x - other.x) <= eps && std::abs(y - other.y) <= eps;
	}
};

std::ostream& operator<<(std::ostream& os, ndc_pos pos) {
	os << "(" << pos.x << "," << pos.y << ")";
	return os;
}

SCENARIO("User needs to map the the grid vertices into the OpenGL NDC space") {
	GIVEN("a 3x2 grid") {
		grid g({3, 2});

		const float slope = 2.0f / 65534;
		const auto map_vertex = [slope](screen_pos input) -> ndc_pos {
			return {
			    -1 + slope * static_cast<float>(input.col - -32767),
			    -1 + slope * static_cast<float>(input.row - -32767),
			};
		};

		THEN("the `rows()` range can be used to access the vertices row by row") {
			std::vector<std::vector<ndc_pos>> points = rows(g) |                                         //
			    view::transform([&map_vertex](auto& row) { return row | view::transform(map_vertex); }); //

			REQUIRE(points.size() == 3);
			const std::vector<ndc_pos> row0{{-1, 1}, {-0.3333f, 1}, {0.3333f, 1}, {1, 1}};
			const std::vector<ndc_pos> row1{{-1, 0}, {-0.3333f, 0}, {0.3333f, 0}, {1, 0}};
			const std::vector<ndc_pos> row2{{-1, -1}, {-0.3333f, -1}, {0.3333f, -1}, {1, -1}};
			REQUIRE(points[0] == row0);
			REQUIRE(points[1] == row1);
			REQUIRE(points[2] == row2);
		}
	}
}

SCENARIO("User needs to calculate the center of every cell in a grid") {
	GIVEN("a 3x2 grid") {
		grid g({3, 2});

		WHEN("the `cells()` function is used to collect the grid cells") {
			const auto grid_cells = cells(g);
			REQUIRE(grid_cells.size() == 6);

			THEN("the cells centers can be calculated") {
				auto center = [](grid_cell c) -> screen_pos {
					return {
					    static_cast<int16_t>(c.top_left.col + (c.bottom_right.col - c.top_left.col) / 2),
					    static_cast<int16_t>(c.top_left.row + (c.bottom_right.row - c.top_left.row) / 2),
					};
				};
				std::vector<screen_pos> cell_centers = grid_cells | view::transform(center);
				const std::vector<screen_pos> centers{
				    {-21845, 16384}, {0, 16384}, {21844, 16384}, {-21845, -16383}, {0, -16383}, {21844, -16383},
				};
				REQUIRE(cell_centers == centers);
			}
		}
	}
}
