#include "wall/coordinates.h"
#include <ostream>

namespace wall {
	std::ostream& operator<<(std::ostream& os, screen_pos pos) {
		os << "[" << pos.col << "," << pos.row << "]";
		return os;
	}

	std::ostream& operator<<(std::ostream& os, point p) {
		os << "[" << p.x << "," << p.y << "]";
		return os;
	}

	std::ostream& operator<<(std::ostream& os, size size) {
		os << "[" << size.width << "," << size.height << "]";
		return os;
	}
}
