#pragma once
#include <bitset>

namespace wall {
	template <std::size_t N>
	class bitset : public std::bitset<N>
	// A small wrapper of a std::bitset that exposes the minimal interface
	// required by `changes_tracker`.
	// The `set()` and `test()` methods are templatized to accept anything that
	// can be converted to a size_t.
	{
	  private:
		using base = std::bitset<N>;

	  public:
		template <typename T>
		void set(T pos) {
			base::set(static_cast<size_t>(pos));
		}

		template <typename T>
		bool test(T pos) {
			return base::test(static_cast<size_t>(pos));
		}
	};

	template <std::size_t N>
	class changes_tracker {
	  public:
		using changes_t = bitset<N>;

		changes_t changes() const {
			return changes_;
		}

		changes_t reset() {
			changes_t out;
			std::swap(changes_, out);
			return out;
		}

	  protected:
		template <typename T>
		void set(T pos) {
			changes_.set(pos);
		}

	  private:
		changes_t changes_;
	};
}
