#pragma once
#include "coordinates.h"
#include "details/matrix.h"
#include "details/opaque.h"
#include <iosfwd>
#include <stdexcept>
#include <vector>

namespace wall {
	struct grid_error : std::runtime_error {
		using std::runtime_error::runtime_error;
	};

	struct invalid_shape : grid_error
	// It is thrown when an attempt to construct a grid with an invalid shape
	// is made.
	{
		invalid_shape() : grid_error{"grid with invalid shape"} {};
	};

	struct invalid_node : grid_error
	// It is thrown when the coordinates of an element of the grid (node, row
	// or column) is outside of the grid extents.
	{
		using grid_error::grid_error;
	};

	struct grid_cell {
		screen_pos top_left;
		screen_pos bottom_left;
		screen_pos bottom_right;
		screen_pos top_right;

		bool operator==(grid_cell other) const;
	};

	std::ostream& operator<<(std::ostream&, grid_cell);

	struct grid_shape {
		uint8_t columns;
		uint8_t rows;

		bool operator==(grid_shape) const;
	};

	std::ostream& operator<<(std::ostream&, grid_shape);

	using col = details::opaque<uint8_t, class column_>;
	using row = details::opaque<uint8_t, class row_>;

	class grid
	/*
	 * A grid is divided by columns and rows.
	 * The number of columns and rows is the shape of the grid.
	 * A vertex is a point where a column and a row intersect
	 *
	 * +--+--+--+--+
	 * |  |  |  |  |
	 * +--+--+--+--+
	 * |  |  |  |  |
	 * +--+--+--+--+
	 * |  |  |  |  |
	 * +--+--+--+--+
	 *
	 * This grid has a shape of 4x3 (4 columns, 3 rows) and 20 vertices.
	*/
	{
	  public:
		using vertices_t = details::matrix<screen_pos>;
		using row_t = vertices_t::row_t;
		using row_iterator = vertices_t::const_iterator;

	  public:
		/// \effects Construct a new grid with the given shape.
		/// \throws invalid_shape() if the shape has zero columns or zero rows.
		grid(grid_shape);

	  public:
		grid_shape size() const;

		/// \returns
		/// The grid cell identified by the given column and row
		///
		/// \throws
		/// std::out_of_range if the column or the row are out of the grid range.
		grid_cell cell(col, row) const;

		/// \returns
		/// The original grid cell identified by the given column and row
		///
		/// \throws
		/// std::out_of_range if the column or the row are out of the grid range.
		///
		/// \details
		/// An original cell is the cell computed using the coordinates of the
		/// rows and the columns before they are changed via set_node
		grid_cell original_cell(col, row) const;

		/// \returns
		/// The screen position of a node
		///
		/// \throws
		/// invalid_node if the node position is outside the grid.
		screen_pos node(node_pos) const;

	  public:
		/// \returns An iterator to the first row of the grid
		row_iterator rows_begin() const;
		/// \returns
		/// An iterator to the element following the last row of the grid
		row_iterator rows_end() const;

	  public:
		/// \effects
		/// The grid nodes are spaced equidistantly, it also reset the original
		/// coordinates for rows and columns.
		void reset();

		/// \effects
		/// A new column is added to the grid shape.
		/// The new column is the result of splitting in half the given one.
		///
		/// \throws
		/// invalid_node if the column is outside the grid.
		/// invalid_shape if the resulting shape exceed the maximum permitted
		void add_col(col);

		/// \effects
		/// A new row is added to the grid shape.
		/// The new row is the result of splitting in half the given one.
		///
		/// \throws
		/// invalid_node if the row is outside the grid.
		/// invalid_shape if the resulting shape exceed the maximum permitted
		void add_row(row);

		/// \effects
		/// The screen position associated with the node is updated to the given value.
		///
		/// \throws
		/// invalid_node if the node is outside the grid.
		/// invalid_shape if the new screen position crosses the neighbours boundary.
		void set_node(node_pos, screen_pos);

	  private:
		uint8_t cols_;
		uint8_t rows_;
		std::vector<screen_pos::type> original_columns_;
		std::vector<screen_pos::type> original_rows_;
		details::matrix<screen_pos> vertices;
	};

	std::ostream& operator<<(std::ostream&, const grid&);

	/// \returns A vector with all the grid cells
	std::vector<grid_cell> cells(const grid&);

	/// \returns A vector with all the original grid cells
	std::vector<grid_cell> original_cells(const grid&);
}
