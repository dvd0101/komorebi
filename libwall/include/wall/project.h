#pragma once
#include "coordinates.h"
#include <iosfwd>

namespace wall {
	class project {
	  public:
		project(size);

		void set_resolution(size);
		size resolution() const;

	  private:
		size resolution_;
	};

	std::ostream& operator<<(std::ostream&, const project&);
}
