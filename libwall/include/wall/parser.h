#pragma once
#include "commands.h"
#include <algorithm>
#include <optional>
#include <string_view>

namespace wall {
	std::optional<command> parse(std::string_view);

	template <typename Iterator, typename Handler>
	Iterator extract_commands(Iterator range_begin, Iterator range_end, Handler& handler)
	// It parses the bytes identified by the passed range and dispatches to the
	// handler the extracted commands; the range can contain zero or more
	// commands, and the last one can be incomplete.
	//
	// All the complete commands are forwarded to the handler, while the
	// incomplete one (if present) is copied to the start of the input range
	//
	// The return value is an iterator pointing to the position inside the
	// buffer where the next input must be appended.
	{
		auto sub_begin = range_begin;
		while (sub_begin != range_end) {
			auto sub_end = std::find(sub_begin, range_end, '\n');
			if (sub_end == range_end) {
				break;
			}

			auto view = std::string_view(&(*sub_begin), std::distance(sub_begin, sub_end));
			auto cmd = wall::parse(view);
			if (cmd) {
				handler.command(*cmd);
			} else {
				handler.parse_failure(view);
			}
			sub_begin = ++sub_end;
		}
		handler.done();

		if (sub_begin != range_end) {
			// the end of the range contains an incomplete command
			if (sub_begin == range_begin) {
				// special case for when the range contains only one incomplete
				// command; this check is used as an optimization (don't copy the
				// entire range over itself) but also to enforce the std::copy
				// preconditions (the output iterator must be outside the source
				// range)
				return range_end;
			}
			return std::copy(sub_begin, range_end, range_begin);
		}
		return range_begin;
	}
}
