#pragma once
#include "coordinates.h"
#include <stdexcept>
#include <string>
#include <variant>

namespace wall {
	namespace commands {
		struct state_change_t {};
		struct state_query_t {};

		struct command_error : std::runtime_error {
			using std::runtime_error::runtime_error;
		};

		struct project_error : command_error {
			project_error();
		};

		struct grid_error : command_error {
			grid_error();
		};

		struct base_state_change {
			using tag = state_change_t;
		};

		struct base_state_query {
			using tag = state_query_t;
		};

		struct add_row : base_state_change
		// adds a new row to the grid after the one specified in the constructor
		{
			uint8_t row;
		};

		struct add_col : base_state_change
		// adds a new col to the grid after the one specified in the constructor
		{
			uint8_t col;
		};

		struct dump_state : base_state_query
		// dump the current state
		{};

		struct load_image : base_state_change
		// load the given image
		{
			std::string file_path;
		};

		struct noop : base_state_query
		// do nothing
		{};

		struct pause : base_state_query
		// pause the given amount of milliseconds
		{
			uint32_t ms;
		};

		class set_grid : public base_state_change
		// defines the size of a grid; an exception is raised if the size
		// is out of range
		{
		  public:
			set_grid(uint8_t, uint8_t);

			uint8_t cols() const;
			uint8_t rows() const;

		  private:
			uint8_t cols_;
			uint8_t rows_;
		};

		struct set_node : base_state_change
		// change the screen position of a grid node
		{
			node_pos node;
			screen_pos screen;
		};

		class set_project : public base_state_change
		// defines the resolution of the project; an exception is raised if the
		// size is out of range
		{
		  public:
			set_project(uint16_t width, uint16_t height);

			uint16_t width() const;
			uint16_t height() const;

		  private:
			uint16_t width_;
			uint16_t height_;
		};

		struct set_projector : base_state_change
		// Set the display origin of a projector. The projector is identified
		// by an optional name, if missing the local projector is intended.
		{
			std::string projector_name;
			int32_t x;
			int32_t y;
		};
	}

	using command = std::variant< //
	    commands::add_col,        //
	    commands::add_row,        //
	    commands::dump_state,     //
	    commands::load_image,     //
	    commands::noop,           //
	    commands::pause,          //
	    commands::set_grid,       //
	    commands::set_node,       //
	    commands::set_project,    //
	    commands::set_projector>; //

	std::string to_string(const command&);
}
