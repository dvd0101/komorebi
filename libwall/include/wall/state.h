#pragma once
#include "changes_tracker.h"
#include "commands.h"
#include "details/opaque.h"
#include "grid.h"
#include "image.h"
#include "project.h"
#include "projector.h"
#include <iosfwd>
#include <optional>

namespace wall {

	namespace changes {
		using change = details::opaque<size_t, class state_change_>;

		constexpr auto project = change(0);
		constexpr auto grid = change(1);
		constexpr auto image = change(2);
		constexpr auto projector = change(3);
	}

	class state : public changes_tracker<4> {
	  public:
		state(const projector_data&);

	  public:
		const wall::project& project() const;
		const wall::projector& local_projector() const;
		const wall::grid& grid() const;
		const wall::image* image() const;

	  public:
		void process(const commands::add_col&);
		void process(const commands::add_row&);
		void process(const commands::load_image&);
		void process(const commands::set_grid&);
		void process(const commands::set_node&);
		void process(const commands::set_project&);
		void process(const commands::set_projector&);

	  private:
		wall::project project_;
		wall::projector local_projector_;
		wall::grid grid_;
		std::optional<wall::image> image_;
	};

	std::optional<std::string> execute(const command&, state&);

	std::ostream& operator<<(std::ostream&, const state&);
	std::string to_string(const state&);
}
