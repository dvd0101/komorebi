#pragma once
#include <cstdint>
#include <gsl/span>
#include <memory>
#include <stdexcept>
#include <string>

namespace wall {
	class image_not_found : public std::runtime_error {
	  public:
		image_not_found() : runtime_error{"image not found"} {}
	};

	class image_load_failed : public std::runtime_error {
      public:
		using std::runtime_error::runtime_error;
	};

	class image
	// A wrapper around the pixels of an image.
	// It must be constructed with a valid path of an image on the filesystem,
	// and the load method can be used to read and decodes the file.
	{
	  public:
		/// \effects Construct a new image
		/// \throws image_not_found() if the given path does not exists
		image(std::string);

		/// \effects loads the pixels from the file used to construct the image
		///
		/// \returns a span to the pixels
		///
		/// \throws image_load_failed if the load fails
		gsl::span<const uint8_t> load();
		/// \returns
		/// A span to the pixels previously loaded.
		/// An empty span is returned if this method is called before load().
		gsl::span<const uint8_t> data() const;

		/// \returns The image width (or zero if the image is not loaded).
		int width() const;
		/// \returns The image height (or zero if the image is not loaded).
		int height() const;

	  public:
		static void release(uint8_t*);

	  private:
		std::string path_;
		std::unique_ptr<uint8_t, decltype(&image::release)> data_;
		int width_;
		int height_;
	};
}
