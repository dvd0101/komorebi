#pragma once
#include "grid.h"
#include <range/v3/all.hpp>

namespace wall {
	class rows : public ranges::view_facade<rows, ranges::finite> {
	  public:
		rows() = default;
		explicit rows(const grid& grid) : it_(grid.rows_begin()), end_(grid.rows_end()) {}

	  private:
		friend ranges::range_access;
		grid::row_iterator it_;
		grid::row_iterator end_;

		const grid::row_t& read() const {
			return *it_;
		}

		bool equal(ranges::default_sentinel) const {
			return it_ == end_;
		}

		bool equal(const rows& other) const {
			return it_ == other.it_;
		}

		void next() {
			++it_;
		}
	};
}
