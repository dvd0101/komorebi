#pragma once

namespace wall::details {
	template <typename T, typename Tag>
	class opaque {
	  public:
		explicit constexpr opaque(T v) : value{v} {}
		opaque(const opaque<T, Tag>& other) : value{other.value} {}

		explicit constexpr operator T() const {
			return value;
		}

		bool operator==(opaque<T, Tag> other) const {
			return value == other.value;
		}

		bool operator!=(opaque<T, Tag> other) const {
			return value != other.value;
		}

	  protected:
		T value;
	};
}
