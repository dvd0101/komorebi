#pragma once
#include <vector>

namespace wall::details {
	template <typename T>
	class matrix {
	  public:
		using storage_t = std::vector<std::vector<T>>;
		using row_t = std::vector<T>;
		using const_iterator = typename storage_t::const_iterator;

	  public:
		matrix() : matrix(0, 0) {}

		matrix(size_t cols, size_t rows) : matrix(cols, rows, [](size_t, size_t) { return T(); }) {}

		template <typename Func>
		matrix(size_t cols, size_t rows, Func&& init) {
			if (cols > 0 && rows > 0) {
				resize(cols, rows, init);
			}
		}

		const_iterator begin() const {
			return cells.begin();
		}

		const_iterator end() const {
			return cells.end();
		}

		size_t cols() const {
			return rows() ? cells[0].size() : 0;
		}

		size_t rows() const {
			return cells.size();
		}

		template <typename Func>
		void resize(size_t cols, size_t rows, Func init) {
			cells.reserve(rows);
			for (size_t r = 0; r < rows; ++r) {
				auto& row = cells.emplace_back();
				row.reserve(cols);
				for (size_t c = 0; c < cols; ++c) {
					row.push_back(init(c, r));
				}
			}
		}

		template <typename Func>
		void add_col(size_t prev, Func&& init) {
			for (size_t r = 0; r < cells.size(); ++r) {
				auto& row = cells[r];
				auto pos = std::begin(row) + prev + 1;
				row.insert(pos, init(r));
			}
		}

		template <typename Func>
		void add_row(size_t prev, Func&& init) {
			auto pos = std::begin(cells) + prev + 1;
			std::vector<T> col;
			col.reserve(cols());
			for (size_t c = 0; c < cols(); ++c) {
				col.push_back(init(c));
			}
			cells.insert(pos, std::move(col));
		}

		const T& get(size_t col, size_t row) const {
			return cells[row][col];
		}

		template <typename Q>
		void set(size_t col, size_t row, Q&& v) {
			cells[row][col] = v;
		}

	  private:
		std::vector<std::vector<T>> cells;
	};
}
