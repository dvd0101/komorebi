#pragma once
#include <cstdint>
#include <iosfwd>
#include <limits>

namespace wall {
	struct node_pos
	// The coordinates of a node inside the grid
	{
		static constexpr uint8_t max = std::numeric_limits<uint8_t>::max();
		uint8_t col;
		uint8_t row;

		constexpr bool operator==(node_pos other) const {
			return col == other.col && row == other.row;
		}
	};

	struct screen_pos
	// The coordinates of a point on the screen.
	// The coordinates are normalized but, unlike opengl, the range is
	// between -32767 to 32767. The integer numbers are used to avoid the
	// unneeded work of serialize and parse floating point numbers.
	{
		using type = int16_t;
		// the minimum value is increased by one to make the resulting range
		// symmetric around the zero
		static constexpr int16_t min = std::numeric_limits<int16_t>::min() + 1;
		static constexpr int16_t max = std::numeric_limits<int16_t>::max();
		static constexpr uint16_t range = max - min;

		constexpr screen_pos() : col{min}, row{max} {}
		constexpr screen_pos(int16_t c, int16_t r) : col{c}, row{r} {}
		int16_t col;
		int16_t row;

		constexpr bool operator==(screen_pos other) const {
			return col == other.col && row == other.row;
		}
	};

	std::ostream& operator<<(std::ostream&, screen_pos);

	struct point
	// A point in pixel coordinates
	{
		int32_t x;
		int32_t y;

		constexpr bool operator==(point other) const {
			return x == other.x && y == other.y;
		}
	};
	std::ostream& operator<<(std::ostream&, point);

	struct size
	// A size in pixel coordinates
	{
		uint16_t width;
		uint16_t height;

		constexpr bool operator==(size other) const {
			return width == other.width && height == other.height;
		}
	};
	std::ostream& operator<<(std::ostream&, size);
}
