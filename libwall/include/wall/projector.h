#pragma once
#include "coordinates.h"
#include <iosfwd>
#include <stdexcept>
#include <string>

namespace wall {
	struct projector_error : std::runtime_error
	// This exception is raised when an invalid `projector_data` or `projector`
	// is constructed.
	{
		using std::runtime_error::runtime_error;
	};

	class projector_data
	// A projector_data contains the metadata about one projector
	{
	  public:
		/// \effects Construct a new projector_data
		/// \throws
		/// projector_error() if the resolution has one of the / dimensions set
		/// to zero.
		projector_data(std::string, size);

		/// \returns the projector name
		const std::string& name() const;
		/// \returns the projector resolution
		size resolution() const;

	  private:
		std::string name_;
		size resolution_;
	};

	class projector : public projector_data
	// A `projector` adds to the metadata inherited from `projector_data` the
	// projection origin
	{
	  public:
		/// \effects Construct a new projector
		/// \throws
		/// projector_error() if the resolution has one of the / dimensions set
		/// to zero.
		projector(std::string, size);

		/// \effects changes the projection origin
		void origin(point);
		/// \returns the projection origin
		point origin() const;

	  private:
		point origin_;
	};

	std::ostream& operator<<(std::ostream&, const projector&);
}
