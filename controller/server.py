from bottle import Bottle, static_file
import bridge

VERSION = "0.1"

bridge.init("../build/bin/wall")

print("controller version:", VERSION)
print("wall version:", bridge.VERSION)

wall = bridge.connect()

app = Bottle()

tpl = """<!doctype html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>komorebi</title>
    <script>const CONTROLLER_VERSION = "%s"</script>
    <script>const WALL_VERSION = "%s"</script>
</head>
<body>
    <div id="app"></div>
    <script src="assets/komorebi.js"></script>
</body>
</html>
""" % (VERSION, bridge.VERSION,)


@app.route("/")
def index():
    return tpl


@app.route("/state")
def state():
    return wall.state()


@app.route("/assets/<filepath:path>")
def assets(filepath):
    return static_file(filepath, root="./assets")

