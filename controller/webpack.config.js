module.exports = {
    entry: "./src/index.ts",
    output: {
        filename: "komorebi.js",
        publicPath: "/assets/",
        path: __dirname + "/assets"
    },

    // Enable sourcemaps for debugging webpack's output.
    devtool: "source-map",

    resolve: {
        // Add '.ts' and '.tsx' as resolvable extensions.
        extensions: [".ts", ".tsx", ".js", ".json"]
    },

    module: {
        rules: [
            // All files with a '.ts' or '.tsx' extension will be handled by 'awesome-typescript-loader'.
            { test: /\.tsx?$/, loader: "ts-loader" },

            // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
            { enforce: "pre", test: /\.js$/, loader: "source-map-loader" },
            {
              test: /src\/.*\.scss$/,
              use: [
                'style-loader',           // creates style nodes from JS strings
                'css-loader',             // resolves url() and @import
                'resolve-url-loader',     // resolves url relative to the scss where they are defined
                'sass-loader?sourceMap',  // compiles Sass to CSS (sourceMap are needed by resolve-url-loader)
              ],
            },
            { test: /src\/components\/.*\.svg?$/, loader: "react-svg-loader" },
            { test: /src\/components\/.*\.png?$/, loader: "file-loader" },
        ]
    },
};
