// the layout of the remote state
type res = [number, number];
type pos = [number, number];
type ndc = [number, number];

export interface ProjectorMetaData {
	name: string;
	resolution: res;
	origin: pos;
}

export interface ProjectorGrid {
	size: {cols: number, rows: number};
	vertices: ndc[][];
}

export interface ProjectorData {
	metadata: ProjectorMetaData;
	grid: ProjectorGrid;
}

export interface ApiState {
	readonly project: {
		readonly resolution: res;
	};
	readonly projectors: ProjectorData[];
}

// an utility function that returns an empty ApiState
export function emptyState(): ApiState {
	return {
		project: {
			resolution: [0, 0],
		},
		projectors: [
			{
				grid: {
					size: {cols: 0, rows: 0},
					vertices: [[[0, 0]]],
				},
				metadata: {name: "local", resolution: [0, 0], origin: [0, 0]},
			},
		],
	};
}

export class KomorebiApi {
	public async state(): Promise<ApiState> {
		return await this.get("/state");
	}

	private async get(url: string) {
		const r = await fetch(url);
		if (!r.ok) {
			throw new Error("api error");
		}
		return await r.json();
	}
}
