import { ApiState, emptyState } from "./api";
import {
	AvailableModules,
	State,
	Step,
} from "./state";

export class KomorebiState implements State {
	private _reactive: () => void;
	private _activeModule: AvailableModules;
	private _timeline: Step[];
	private _wall: ApiState;

	constructor(f: () => void) {
		this._reactive = f;
		this._timeline = [];
		this._activeModule = "project";
		this._wall = emptyState();
	}

	get timeline() {
		return this._timeline;
	}

	set timeline(value: Step[]) {
		this._timeline = value;
		this._reactive();
	}

	get activeModule() {
		return this._activeModule;
	}

	set activeModule(value: AvailableModules) {
		this._activeModule = value;
		this._reactive();
	}

	get wall() {
		return this._wall;
	}

	set wall(s: ApiState) {
		this._wall = s;
		this._reactive();
	}
}
