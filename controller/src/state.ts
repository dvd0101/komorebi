import { ApiState } from "./api";

export class Step {
	public image: string;

	// Step duration in seconds
	public duration: number;
}

export type AvailableModules = "project" | "timeline" | "gallery" | "settings";

export interface State {
	readonly activeModule: AvailableModules;
	readonly timeline: Step[];
	readonly wall: ApiState;
}
