export function prevent(f: any, stopPropagation = false) {
	return (evt: any) => {
		evt.preventDefault();
		if (stopPropagation) {
			evt.stopPropagation();
		}
		f();
	};
}
