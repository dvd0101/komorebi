import * as React from "react";
import * as ReactDOM from "react-dom";
import { KomorebiApi } from "./api";
import { Main } from "./components/main/index";
import { KomorebiState } from "./komorebi_state";
import { AvailableModules } from "./state";

require("./style.scss");

export class App {
	private state: KomorebiState;
	private api: KomorebiApi;

	constructor() {
		this.api = new KomorebiApi();
		this.state = new KomorebiState(() => this.render());
		this.state.timeline = [
			{
				duration: 10,
				image: "gattino.jpg",
			},
			{
				duration: 5,
				image: "unicorno.jpg",
			},
			{
				duration: 5,
				image: "gattino.jpg",
			},
			{
				duration: 5,
				image: "unicorno.jpg",
			},
		];
		this.syncState();
	}

	public render() {
		const el = document.getElementById("app");
		ReactDOM.render((
			<Main
				state={this.state}

				onChangeActiveModule={this.onChangeActiveModule}
				onTimelineAddEmptyStep={this.onTimelineAddEmptyStep}
			/>
		), el);
	}

	private onChangeActiveModule = (m: AvailableModules) => {
		this.state.activeModule = m;
	}

	private onTimelineAddEmptyStep = () => {
		this.state.timeline = this.state.timeline.concat([{duration: 1, image: "gattino.jpg"}]);
	}

	// updates the local state with the remote data
	private async syncState() {
		this.state.wall = await this.api.state();
	}
}
