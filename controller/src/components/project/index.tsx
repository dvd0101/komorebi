import * as React from "react";
import { ApiState } from "../../api";
import Projector from "./projector";

require("./style.scss");

interface TimelineProps {
	wall: ApiState;
}

export class Project extends React.Component<TimelineProps, {}> {
	public render() {
		return (
			<div className="project">
				{this.renderProjectArea()}
			</div>
		);
	}

	private renderProjectArea() {
		const [width, height ] = this.props.wall.project.resolution;
		return (
			<svg
				viewBox={`0 0 ${width} ${height}`}
				xmlns="http://www.w3.org/2000/svg"
				xmlnsXlink="http://www.w3.org/1999/xlink">
				{this.renderProjectBackground()}
				{this.props.wall.projectors.map((p, ix) => <Projector p={p} key={ix} />)}
			</svg>
		);
	}

	private renderProjectBackground() {
		const [width, height ] = this.props.wall.project.resolution;
		return (
			<g className="project__background">
				<rect x="0" y="0" width={width} height={height} />
				<text x="50%" y="50%" textAnchor="middle">{`${width}x${height}`}</text>
			</g>
		);
	}
}
