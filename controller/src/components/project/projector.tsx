import * as React from "react";
import { ProjectorData, ProjectorGrid } from "../../api";

interface GridProps {
	grid: ProjectorGrid;
	resolution: [number, number];
	// vertexDragged is used to draw a vertex during the drag
	// vertexDragged is decomposed to [row, col, xOffset, yOffset]
	vertexDragged: [number, number, number, number] | null;
}

interface GridState {
	// the vertices in pixel coordinate
	vertices: Array<Array<[number, number]>>;
}

class Grid extends React.Component<GridProps, GridState> {
	constructor(props: GridProps) {
		super(props);
		this.state = {
			vertices: this._calcVerticies(this.props.resolution, this.props.grid.vertices),
		};
	}

	public componentWillReceiveProps(nextProps: GridProps) {
		// TODO: don't be lazy and check if nextProps is different from this.props
		this.setState({
			vertices: this._calcVerticies(nextProps.resolution, nextProps.grid.vertices),
		});
	}

	public render() {
		this._rows();
		return (
			<g className="grid">
				<g className="rows">
					{this._rows()}
				</g>
				<g className="cols">
					{this._cols()}
				</g>
				<g className="vertices">
					{this._vertices()}
				</g>
			</g>
		);
	}

	private _calcVerticies(res: [number, number], ndc: Array<Array<[number, number]>>) {
		const pixels: Array<Array<[number, number]>> = [];
		const slopeX = res[0]  / (32767 - -32767);
		const slopeY = res[1]  / (32767 - -32767);

		const ndcToPixel = (x: number, y: number): [number, number] => {
			return [
				slopeX * (1 * x - -32767),
				slopeY * (-1 * y - -32767),
			];
		};

		for (const row of ndc) {
			pixels.push(row.map((v) => ndcToPixel(v[0], v[1])));
		}
		return pixels;
	}

	private _cols() {
		const lines: JSX.Element[] = [];

		for (let colx = 0; colx <= this.props.grid.size.cols; colx++) {
			const points: string[] = [];
			for (let rowx = 0; rowx <= this.props.grid.size.rows; rowx++) {
				const [x, y] = this._vertexPos(rowx, colx);
				points.push(`${x},${y}`);
			}
			lines.push(<polyline key={`col-${lines.length}`} points={points.join(" " )} />);
		}

		return lines;
	}

	private _rows() {
		const lines: JSX.Element[] = [];

		for (let rowx = 0; rowx <= this.props.grid.size.rows; rowx++) {
			const points: string[] = [];
			for (let colx = 0; colx <= this.props.grid.size.cols; colx++) {
				const [x, y] = this._vertexPos(rowx, colx);
				points.push(`${x},${y}`);
			}
			lines.push(<polyline key={`col-${lines.length}`} points={points.join(" " )} />);
		}

		return lines;
	}

	private _vertices() {
		const rects: JSX.Element[] = [];

		this.state.vertices.forEach((row, rowIx) => {
			row.forEach((v, colIx) => {
				const [x, y] = this._vertexPos(rowIx, colIx);
				rects.push(<rect
					key={`v-${rects.length}`}
					x={x - 5}
					y={y - 5}
					width="10"
					height="10"
					data-row={rowIx}
					data-col={colIx}
				/>);
			});
		});

		return rects;
	}

	private _vertexPos(row: number, col: number): [number, number] {
		const p = this.state.vertices[row][col];
		if (this.props.vertexDragged) {
			const [ r, c, dx, dy ] = this.props.vertexDragged;
			if (r === row && c === col) {
				return [p[0] + dx , p[1] + dy];
			}
		}
		return p;
	}
}

interface ProjectorProps {
	p: ProjectorData;
}

interface ProjectorState {
	dragOffset: [number, number];
	dragStart: [number, number] | null;
	dragTarget: "projector" | "vertex" | null;
	// if dragTarget is "vertex" `vertexId` is the vertex coordinate [row, col]
	vertexId: [number, number];
}

export default class Projector extends React.Component<ProjectorProps, ProjectorState> {
	constructor(props: ProjectorProps) {
		super(props);
		this.state = {
			dragOffset: [0, 0],
			dragStart: null,
			dragTarget: null,
			vertexId: [0, 0],
		};
	}

	public render() {
		const p = this.props.p.metadata;
		return (
			<svg
				className="projector"
				x={p.origin[0] + (this.state.dragTarget === "projector" ? this.state.dragOffset[0] : 0)}
				y={p.origin[1] + (this.state.dragTarget === "projector" ? this.state.dragOffset[1] : 0)}
				width={p.resolution[0]}
				height={p.resolution[1]}
				viewBox={`0 0 ${p.resolution[0]} ${p.resolution[1]}`}
				xmlns="http://www.w3.org/2000/svg"
				xmlnsXlink="http://www.w3.org/1999/xlink"
				key={p.name}
				onMouseDown={this.onMouseDown}
				onMouseMove={this.onMouseMove}
				onMouseUp={this.onMouseUp}
			>
				<rect x="0" y="0" width="100%" height="100%" />
				<text x="50%" y="50%" textAnchor="middle">{p.name}</text>
				<text x="50%" y="70%" textAnchor="middle">{`${p.resolution[0]}x${p.resolution[1]}`}</text>
				<Grid
					grid={this.props.p.grid}
					resolution={p.resolution}
					vertexDragged={
						this.state.dragTarget === "vertex"
						? [this.state.vertexId[0], this.state.vertexId[1], this.state.dragOffset[0], this.state.dragOffset[1]]
						: null
					}
				/>
			</svg>
		);
	}

	private onMouseDown = (evt: React.MouseEvent<SVGSVGElement>) => {
		const el = evt.target as HTMLElement;
		const vRow = el.dataset.row;
		const vCol = el.dataset.col;
		const t = (vRow !== undefined && vCol !== undefined) ? "vertex" : "projector";
		this.setState({
			dragOffset: [0, 0],
			dragStart: [evt.clientX, evt.clientY],
			dragTarget: t,
			vertexId: t === "vertex" ? [Number(vRow), Number(vCol)] : [0, 0],
		});
	}

	private onMouseMove = (evt: React.MouseEvent<SVGSVGElement>) => {
		if (this.state.dragStart) {
			const [sx, sy] = this.state.dragStart;
			this.setState({
				dragOffset: [evt.clientX - sx, evt.clientY - sy],
			});
		}
	}

	private onMouseUp = (evt: React.MouseEvent<SVGSVGElement>) => {
		console.log("committing", this.state.dragTarget);
		this.setState({
			dragStart: null,
			dragTarget: null,
		});
	}
}
