import * as classNames from "classnames";
import * as React from "react";
import { AvailableModules, State } from "../../state";
import { prevent } from "../../utils";

require("./style.scss");

import Gallery from "./assets/gallery.svg";
import Project from "./assets/project.svg";
import Settings from "./assets/settings.svg";
import Timeline from "./assets/timeline.svg";

const logo = require("./assets/komorebi.png");

interface AppMenuProps {
	state: State;
	onChangeActiveModule: (m: AvailableModules) => void;
}

export class AppMenu extends React.Component<AppMenuProps, {}> {
	public render() {
		const m = this.props.state.activeModule;
		return (
			<div className="app-menu">
				<div>
					<img src={logo} />
				</div>
				{ ["project", "timeline", "gallery", "settings"].map((x: AvailableModules) => this.element(x)) }
			</div>
		);
	}

	private element(id: AvailableModules) {
		return (
			<div key={id} className={classNames({active: this.props.state.activeModule === id})}>
				<a href="#" onClick={prevent(() => this.props.onChangeActiveModule(id))}>
					{ id === "gallery" && <Gallery />}
					{ id === "project" && <Project />}
					{ id === "settings" && <Settings />}
					{ id === "timeline" && <Timeline />}
					<span>{id}</span>
				</a>
			</div>
		);
	}
}
