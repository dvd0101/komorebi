import * as React from "react";
import { Step } from "../../state";

interface SingleStepProps {
	step: Step;
}

export class SingleStep extends React.Component<SingleStepProps, {}> {
	public render() {
		const step = this.props.step;
		return (
			<div className="single-step">
				<div className="single-step__image">
					<img src={`/assets/${step.image}`} />
				</div>
			</div>
		);
	}
}
