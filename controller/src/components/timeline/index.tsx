import * as React from "react";
import { Step } from "../../state";
import { SingleStep } from "./singlestep_view";

require("./style.scss");

interface TimelineProps {
	steps: Step[];
	onAddEmptyStep: () => void;
}

export class Timeline extends React.Component<TimelineProps, {}> {
	public render() {
		const elements = this.props.steps.map((el, index) => {
			return <SingleStep key={index} step={el} />;
		});
		return (
			<div className="timeline">
				<div className="timeline__steps">
					<div className="timeline__steps--start">START</div>
					{elements}
					<div
						className="timeline__steps--end"
						onClick={this.onAddClick}
					>ADD</div>
				</div>
			</div>
		);
	}

	private onAddClick = (evt: React.MouseEvent<HTMLDivElement>) => {
		evt.preventDefault();
		this.props.onAddEmptyStep();
	}
}
