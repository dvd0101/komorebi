import * as React from "react";
import { AvailableModules, State } from "../../state";
import { AppMenu } from "../app_menu/index";
import { Project } from "../project/index";
import { Timeline } from "../timeline/index";

require("./style.scss");

interface MainProps {
	state: State;
	onChangeActiveModule: (m: AvailableModules) => void;
	onTimelineAddEmptyStep: () => void;
}

export class Main extends React.Component<MainProps, {}> {
	public render() {
		const state = this.props.state;
		return (
			<div className="main">
				<AppMenu {...this.props} />
				<div className="content">
					{ state.activeModule === "project" &&
						<Project wall={state.wall} /> }
					{ state.activeModule === "timeline" &&
						<Timeline steps={state.timeline} onAddEmptyStep={this.props.onTimelineAddEmptyStep} /> }
				</div>
			</div>
		);
	}
}
