import json
import subprocess

_EXE = None

VERSION = None


def init(exe_name):
    global _EXE
    global VERSION
    r = subprocess.run([exe_name, "--version"], stdout=subprocess.PIPE)
    _EXE = exe_name
    VERSION = r.stdout.strip().decode("utf-8")


class PipeError(Exception):
    pass


class CommandError(PipeError):
    pass


class ProtocolError(PipeError):
    pass


class Pipe:
    @classmethod
    def open(cls, exe_name):
        pipe = subprocess.Popen(
            args=[exe_name, "--pipe"],
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            encoding="utf-8",
        )
        return cls(pipe)

    def __init__(self, pipe):
        self.pipe = pipe

    def state(self):
        return json.loads(self._send_command("DUMP"))

    def _send_command(self, cmd):
        self.pipe.stdin.write(cmd + "\n")
        self.pipe.stdin.flush()
        line = self.pipe.stdout.readline()
        if line.startswith("+OK "):
            return line[4:]
        elif line.startswith("+ERR "):
            raise CommandError(line[5:])
        else:
            raise ProtocolError("response: " + line)


def connect():
    assert _EXE
    return Pipe.open(_EXE)
