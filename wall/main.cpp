#include "cmdline.h"
#include "input_thread.h"
#include "io.h"
#include "opengl/context.h"
#include "opengl/ui.h"
#include "wall/state.h"
#include <chrono>
#include <iostream>
#include <thread>

int main(int argc, char* argv[]) {
	const auto opt = argparse(argc, argv);
	std::unique_ptr<io_writer> io;
	if (opt.pipe_mode) {
		io = std::make_unique<pipe_io_writer>();
	} else {
		io = std::make_unique<human_io_writer>();
	}

	// the thread that consumes the stdint; the programm will end when the
	// intput reaches the eof.
	auto[input_thread, input_complete] = start_input_thread();

	// our local projector
	wall::projector_data local("local", {800, 600});

	opengl::context ctx(opengl::window_size(local.resolution()));
	opengl::ui ui(ctx);

	// the only mutable reference to the app state
	wall::state state(local);

	wall::state::changes_t prev_changed;
	std::future_status input_status;
	do {
		const auto commands = pending_commands();
		for (auto cmd : commands) {
			const auto out = execute(cmd, state);
			if (out) {
				io->log(*out);
			}
			io->write();
		}

		const auto what_changed = state.changes();
		if (prev_changed != what_changed) {
			ui.update(state, what_changed);
			prev_changed = what_changed;
			state.reset();
		}
		ui.draw(state);
		input_status = input_complete.wait_for(std::chrono::milliseconds(1));
	} while (input_status != std::future_status::ready);

	// not necessary anymore, but it prevent the compiler to complain about
	// `input_thread` be unused :)
	input_thread.join();
}
