#pragma once
#include <optional>
#include <string>

class io_writer {
  public:
	virtual ~io_writer();

	void log(std::string msg="");
  	void err(std::string msg="");
	void write();

  private:
  	virtual void emit_log(std::string) = 0;
  	virtual void emit_err(std::string) = 0;

  private:
	std::string log_message_;
	std::optional<std::string> err_message_;
};

class human_io_writer : public io_writer {
  private:
  	virtual void emit_log(std::string) override;
  	virtual void emit_err(std::string) override;
};

class pipe_io_writer : public io_writer {
  private:
  	virtual void emit_log(std::string) override;
  	virtual void emit_err(std::string) override;
};
