#pragma once
#include "handle.h"
#include "wall/image.h"

namespace opengl {
	class texture_handle : public handle<texture_handle> {
	  public:
		static gl::GLuint acquire() {
			using namespace gl;
			GLuint id;
			glGenTextures(1, &id);
			return id;
		}

		static void release(gl::GLuint id) {
			gl::glDeleteTextures(1, &id);
		}

		texture_handle() : handle{acquire()} {}
	};

	inline auto bind(gl::GLenum target, const texture_handle& h) {
		gl::glBindTexture(target, h);
		return binder{[=] { gl::glBindTexture(target, 0); }};
	}

	inline void upload(const texture_handle& h, const wall::image& img) {
		const auto lock = bind(gl::GL_TEXTURE_2D, h);
		const auto pixels = img.data();
		gl::glTexImage2D(gl::GL_TEXTURE_2D,
		                 0,
		                 gl::GL_RGB,
		                 img.width(),
		                 img.height(),
		                 0,
		                 gl::GL_RGB,
		                 gl::GL_UNSIGNED_BYTE,
		                 pixels.data());
		gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_MAG_FILTER, gl::GL_LINEAR);
		gl::glTexParameteri(gl::GL_TEXTURE_2D, gl::GL_TEXTURE_MIN_FILTER, gl::GL_LINEAR);
	}
}
