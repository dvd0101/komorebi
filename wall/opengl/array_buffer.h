#pragma once
#include "handle.h"
#include <gsl/span>

namespace opengl {
	class buffer_handle : public handle<buffer_handle> {
	  public:
		static gl::GLuint acquire() {
			using namespace gl;
			GLuint id;
			glGenBuffers(1, &id);
			return id;
		}

		static void release(gl::GLuint id) {
			gl::glDeleteBuffers(1, &id);
		}

		buffer_handle() : handle{acquire()} {}
	};

	inline auto bind(const buffer_handle& h) {
		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, h);
		return binder{[&] { gl::glBindBuffer(gl::GL_ARRAY_BUFFER, 0); }};
	}

	inline void upload(const buffer_handle& h, gsl::span<float> values) {
		const auto lock = bind(h);
		gl::glBufferData(gl::GL_ARRAY_BUFFER, values.size_bytes(), values.data(), gl::GL_STATIC_DRAW);
	}
}
