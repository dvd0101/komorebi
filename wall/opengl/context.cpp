#include "context.h"
#include <glbinding/Binding.h>
#include <iostream>

struct glfw_library {
	glfw_library() {
		init_ = glfwInit() == GLFW_TRUE;
		glfwSetErrorCallback([](int error, const char* description) {
			std::cerr << "GLFW ERROR: " << error << " " << description << "\n";
		});
	}

	~glfw_library() {
		if (init_) {
			glfwTerminate();
		}
	}

	bool init_;
};

namespace {
	const glfw_library glfw_handle;
}

using namespace gl;
namespace {
	struct Debug_message {
		GLenum source_;
		GLenum type_;
		GLuint id_;
		GLenum severity_;

		char const* source() const {
			switch (source_) {
			case GL_DEBUG_SOURCE_API:
				return "GL_DEBUG_SOURCE_API";
			case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
				return "GL_DEBUG_SOURCE_WINDOW_SYSTEM";
			case GL_DEBUG_SOURCE_SHADER_COMPILER:
				return "GL_DEBUG_SOURCE_SHADER_COMPILER";
			case GL_DEBUG_SOURCE_THIRD_PARTY:
				return "GL_DEBUG_SOURCE_THIRD_PARTY";
			case GL_DEBUG_SOURCE_APPLICATION:
				return "GL_DEBUG_SOURCE_APPLICATION";
			case GL_DEBUG_SOURCE_OTHER:
				return "GL_DEBUG_SOURCE_OTHER";
			default:
				return "Unknown";
			}
		}

		char const* type() const {
			switch (type_) {
			case GL_DEBUG_TYPE_ERROR:
				return "GL_DEBUG_TYPE_ERROR";
			case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
				return "GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR";
			case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
				return "GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR";
			case GL_DEBUG_TYPE_PORTABILITY:
				return "GL_DEBUG_TYPE_PORTABILITY";
			case GL_DEBUG_TYPE_PERFORMANCE:
				return "GL_DEBUG_TYPE_PERFORMANCE";
			case GL_DEBUG_TYPE_MARKER:
				return "GL_DEBUG_TYPE_MARKER";
			case GL_DEBUG_TYPE_PUSH_GROUP:
				return "GL_DEBUG_TYPE_PUSH_GROUP";
			case GL_DEBUG_TYPE_POP_GROUP:
				return "GL_DEBUG_TYPE_POP_GROUP";
			case GL_DEBUG_TYPE_OTHER:
				return "GL_DEBUG_TYPE_OTHER";
			default:
				return "Unknown";
			}
		}

		char const* severity() const {
			switch (severity_) {
			case GL_DEBUG_SEVERITY_HIGH:
				return "GL_DEBUG_SEVERITY_HIGH";
			case GL_DEBUG_SEVERITY_MEDIUM:
				return "GL_DEBUG_SEVERITY_MEDIUM";
			case GL_DEBUG_SEVERITY_LOW:
				return "GL_DEBUG_SEVERITY_LOW";
			case GL_DEBUG_SEVERITY_NOTIFICATION:
				return "GL_DEBUG_SEVERITY_NOTIFICATION";
			default:
				return "Unknown";
			}
		}
	};

	std::ostream& operator<<(std::ostream& os, Debug_message const& msg) {
		os << "source=" << msg.source() << " type=" << msg.type() << " id=" << msg.id_
		   << " severity=" << msg.severity();
		return os;
	}

	void context_debug(
	    GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei, const GLchar* message, const void*) {
		if (severity > GL_DEBUG_SEVERITY_NOTIFICATION) {
			Debug_message msg{source, type, id, severity};
			std::cerr << msg << " - " << message << "\n";
		}
	}
}

namespace opengl {
	context::context(window_size size) : window_{nullptr} {
		glfwWindowHint(GLFW_DOUBLEBUFFER, 1);
		glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_ES_API);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
		glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, 1);
		window_ = glfwCreateWindow(size.width, size.height, "wall", nullptr, nullptr);

		if (window_) {
			glfwMakeContextCurrent(window_);
			glbinding::Binding::initialize();

			using namespace glbinding;
			setCallbackMaskExcept(CallbackMask::After, {"glGetError"});
			setAfterCallback([](const FunctionCall&) {
				const auto error = glGetError();
				if (error != GL_NO_ERROR)
					std::cout << "error: " << std::hex << error << std::endl;
			});

			glEnable(GL_DEBUG_OUTPUT);
			glDebugMessageCallback(context_debug, nullptr);
		}
	}

	context::~context() {
		if (window_) {
			glfwDestroyWindow(window_);
		}
	}

	context::operator GLFWwindow*() {
		return window_;
	}
}
