#pragma once
#include "opengl.h"

namespace opengl {
	template <typename Derived>
	class handle {
	  public:
		explicit handle(gl::GLuint v) : handle_{v} {}

		handle(const handle&) = delete;
		handle(handle&& other) : handle{other.handle_} {
			other.handle_ = 0;
		}

		handle& operator=(const handle&) = delete;
		handle& operator=(const handle&& other) {
			handle_ = other.handle_;
			other.handle_ = 0;
		}

		~handle() {
			if (handle_ != 0) {
				Derived::release(handle_);
			}
		}

		operator gl::GLuint() const {
			return handle_;
		}

	  private:
		gl::GLuint handle_;
	};

	template <typename T>
	class binder {
	  public:
		template <typename F = T>
		binder(F&& func) : func_{std::forward<F>(func)} {}

		~binder() {
			func_();
		}

	  private:
		T func_;
	};

	template <typename T>
	binder(T &&)->binder<T>;
}
