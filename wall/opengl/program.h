#pragma once
#include "handle.h"

namespace opengl {

	class program_handle : public handle<program_handle> {
	  public:
		static void release(gl::GLuint id) {
			gl::glDeleteProgram(id);
		}

		using handle::handle;
	};

	program_handle assemble(const char* vertex, const char* fragment);
	inline auto bind(const program_handle& h) {
		gl::glUseProgram(h);
		return binder{[&] { gl::glUseProgram(0); }};
	}
}
