#include "program.h"
#include <cstdlib>
#include <iostream>

namespace opengl {
	program_handle assemble(const char* vertex, const char* fragment) {
		using namespace gl;

		GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vertex_shader, 1, &vertex, NULL);
		glCompileShader(vertex_shader);

		GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(fragment_shader, 1, &fragment, NULL);
		glCompileShader(fragment_shader);

		program_handle program(glCreateProgram());
		glAttachShader(program, vertex_shader);
		glAttachShader(program, fragment_shader);
		glLinkProgram(program);

		GLboolean status;
		glGetProgramiv(program, GL_LINK_STATUS, &status);
		if (status == GL_FALSE) {
			std::cerr << "GSLS progam link failed\n";
			char msg[1024];
			int length;
			glGetProgramInfoLog(program, 1024, &length, msg);
			std::cerr << msg << "\n";
			abort();
		}

		glDeleteShader(vertex_shader);
		glDeleteShader(fragment_shader);

		return program;
	}
}
