#include "ui.h"
#include "array_buffer.h"
#include "programs/primitives.h"
#include "programs/project.h"
#include "texture.h"
#include "wall/coordinates.h"
#include "wall/grid_range.h"

using namespace gl;
using namespace ranges;

namespace {
	struct texture_unit {};
	struct ndc_pos {
		static constexpr float slope = 2.0f / wall::screen_pos::range;

		ndc_pos(float nx, float ny) : x{nx}, y{ny} {}
		ndc_pos(wall::screen_pos pos) {
			x = -1 + slope * static_cast<float>(pos.col - wall::screen_pos::min);
			y = -1 + slope * static_cast<float>(pos.row - wall::screen_pos::min);
		}
		ndc_pos(wall::screen_pos pos, texture_unit) : ndc_pos(pos) {
			x = (x + 1) / 2;
			y = (y - 1) / -2;
		}

		float x;
		float y;
	};
	static_assert(sizeof(ndc_pos) == 2 * sizeof(float), "ndc_pos is padded");
}

namespace opengl {
	const float red[]{1.f, 0.f, 0.f};
	const float blue[]{0.f, 0.f, 1.f};
	const float yellow[]{1.f, 1.f, 0.f};

	class drawing_state {
	  public:
		buffer_handle grid_lines;
		buffer_handle grid_cells;
		buffer_handle texture_coordinates;
		texture_handle image;
		project_image project;
		primitives lines;
		primitives triangles;

		void upload_lines(const wall::grid& grid) {
			// clang-format off
			std::vector<ndc_pos> coords = wall::rows(grid)
				| view::for_each([](auto& row) {
					return row | view::transform([](auto& pos) { return ndc_pos(pos); });
				});
			// clang-format on

			const auto row_count = grid.size().rows + 1;
			const size_t start_columns = coords.size();
			coords.resize(coords.size() * 2, ndc_pos(0, 0));
			size_t rx = 0;
			for (const auto& row : wall::rows(grid)) {
				for (size_t cx = 0; cx < row.size(); ++cx) {
					coords[start_columns + cx * row_count + rx] = ndc_pos(row[cx]);
				}
				++rx;
			}
			upload(grid_lines, {reinterpret_cast<float*>(coords.data()), static_cast<long>(coords.size() * 2)});
		}

		void upload_cells(const wall::grid& grid) {
			std::vector<ndc_pos> triangles;
			for (const auto& cell : wall::cells(grid)) {
				triangles.emplace_back(cell.top_left);
				triangles.emplace_back(cell.bottom_left);
				triangles.emplace_back(cell.bottom_right);

				triangles.emplace_back(cell.top_left);
				triangles.emplace_back(cell.top_right);
				triangles.emplace_back(cell.bottom_right);
			}
			upload(grid_cells, {reinterpret_cast<float*>(triangles.data()), static_cast<long>(triangles.size() * 6)});

			std::vector<ndc_pos> coords;
			for (const auto cell : wall::original_cells(grid)) {
				coords.push_back(ndc_pos(cell.top_left, texture_unit{}));
				coords.push_back(ndc_pos(cell.bottom_left, texture_unit{}));
				coords.push_back(ndc_pos(cell.bottom_right, texture_unit{}));

				coords.push_back(ndc_pos(cell.top_left, texture_unit{}));
				coords.push_back(ndc_pos(cell.top_right, texture_unit{}));
				coords.push_back(ndc_pos(cell.bottom_right, texture_unit{}));
			}
			upload(texture_coordinates,
			       {reinterpret_cast<float*>(coords.data()), static_cast<long>(coords.size() * 2)});
		}

		void upload_image(const wall::image& img) {
			upload(image, img);
			gl::glActiveTexture(gl::GL_TEXTURE0);
			gl::glBindTexture(gl::GL_TEXTURE_2D, image);
			project.set_texture_unit(gl::GL_TEXTURE0);
		}

		void upload_projection_area(const wall::project& prj, const wall::projector& proj) {
			project.set_project_ratio(prj, proj);
		}
	};

	void draw_grid_lines(drawing_state& dstate, const wall::grid& grid) {
		const auto shape = grid.size();
		for (uint8_t row = 0; row <= shape.rows; ++row) {
			dstate.lines.draw(GL_LINE_STRIP, dstate.grid_lines, row * (shape.columns + 1), shape.columns + 1);
		}

		const auto first_column = shape.rows * (shape.columns + 1) + shape.columns + 1;
		for (uint8_t col = 0; col <= shape.columns; ++col) {
			dstate.lines.draw(GL_LINE_STRIP, dstate.grid_lines, first_column + col * (shape.rows + 1), shape.rows + 1);
		}
	}

	void draw_grid_cells(drawing_state& dstate, const wall::grid& grid) {
		const auto shape = grid.size();
		dstate.project.draw(dstate.grid_cells, dstate.texture_coordinates, shape.rows * shape.columns * 6);
	}

	void draw_grid_cells_debug(drawing_state& dstate, const wall::grid& grid) {
		const auto shape = grid.size();
		dstate.triangles.set_color(blue);
		for (int i = 0; i < shape.columns * shape.rows; ++i) {
			dstate.triangles.draw(GL_TRIANGLES, dstate.grid_cells, i * 6, 3);
			break;
		}
		dstate.triangles.set_color(yellow);
		for (int i = 0; i < shape.columns * shape.rows; ++i) {
			dstate.triangles.draw(GL_TRIANGLES, dstate.grid_cells, 3 + i * 6, 3);
			break;
		}
	}
}

namespace opengl {

	ui::ui(context& ctx) : ctx_{&ctx}, dstate_{new drawing_state} {
		glfwSwapInterval(1);
	}
	ui::~ui() = default;

	void ui::update(const wall::state& state, wall::state::changes_t what_changed) {
		if (what_changed.test(wall::changes::grid)) {
			dstate_->upload_lines(state.grid());
			dstate_->upload_cells(state.grid());
		}
		if (what_changed.test(wall::changes::image)) {
			dstate_->upload_image(*state.image());
		}
		if (what_changed.test(wall::changes::project) || what_changed.test(wall::changes::projector)) {
			dstate_->upload_projection_area(state.project(), state.local_projector());
		}
	}

	bool ui::draw(const wall::state& state) {
		glClear(GL_COLOR_BUFFER_BIT);

		if (state.image()) {
			draw_grid_cells(*dstate_, state.grid());
		}
		draw_grid_lines(*dstate_, state.grid());
		// draw_grid_cells_debug(*dstate_, state.grid());

		glfwSwapBuffers(*ctx_);

		glfwPollEvents();
		return !glfwWindowShouldClose(*ctx_);
	}
}
