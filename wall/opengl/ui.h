#pragma once
#include "context.h"
#include "wall/state.h"
#include <memory>

namespace opengl {
	class drawing_state;

	class ui {
	  public:
		ui(context&);
		~ui();

		void update(const wall::state&, wall::state::changes_t);
		bool draw(const wall::state&);

	  private:
		context* ctx_;
		std::unique_ptr<drawing_state> dstate_;
	};
}
