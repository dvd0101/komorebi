#pragma once
#include "opengl.h"
#include <wall/coordinates.h>

namespace opengl {
	struct window_size {
		int width;
		int height;

		window_size(int w, int h) : width{w}, height{h} {}

		// implicit conversion from wall::size
		window_size(wall::size s): width{s.width}, height{s.height} {}
	};

	class context {
	  public:
		context(window_size);
		~context();

		operator GLFWwindow*();

	  private:
		GLFWwindow* window_;
	};
}
