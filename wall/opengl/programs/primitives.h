#pragma once
#include "../array_buffer.h"
#include "../program.h"

namespace opengl {
	class primitives {
	  public:
		primitives();

		void set_color(const float[3]);
		void draw(gl::GLenum, const buffer_handle&, gl::GLint, gl::GLsizei);

	  private:
		program_handle prg_;
		struct {
			gl::GLint color;
		} uniforms_;
		gl::GLint vertex_loc_;
	};
}


