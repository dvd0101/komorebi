#include "project.h"
#include "wall/project.h"
#include "wall/projector.h"

namespace {
	const char* vertex_shader = R"(
#version 100
uniform vec2 proj_origin;
uniform vec2 area_ratio;

attribute vec2 vertex;
attribute vec2 image_coord;
varying highp vec2 shifted_image_coord;

void main() {
	gl_Position = vec4(vertex, 0.0, 1.0);
	shifted_image_coord = proj_origin + image_coord;
})";

	const char* fragment_shader = R"(
#version 100
uniform sampler2D image_texture;

varying highp vec2 shifted_image_coord;

void main() {
	gl_FragColor = texture2D(image_texture, shifted_image_coord);
})";
}

using namespace gl;

namespace opengl {
	project_image::project_image()
	    : prg_{assemble(vertex_shader, fragment_shader)},
	      uniforms_{
	          glGetUniformLocation(prg_, "image_texture"),
	          glGetUniformLocation(prg_, "proj_origin"),
	          glGetUniformLocation(prg_, "area_ratio"),
	      },
	      vertex_loc_{glGetAttribLocation(prg_, "vertex")},
	      image_coord_loc_{glGetAttribLocation(prg_, "image_coord")} {}

	void project_image::set_texture_unit(gl::GLenum unit) {
		const auto lock_prg = bind(prg_);
		glUniform1i(uniforms_.image_texture, static_cast<int>(unit) - static_cast<int>(GL_TEXTURE0));
	}

	void project_image::set_project_ratio(const wall::project& project, const wall::projector& projector) {
		const auto lock_prg = bind(prg_);
		const float rw = static_cast<float>(projector.resolution().width) / project.resolution().width;
		const float rh = static_cast<float>(projector.resolution().height) / project.resolution().height;
		glUniform2f(uniforms_.area_ratio, rw, rh);

		const float rx = static_cast<float>(projector.origin().x) / project.resolution().width;
		const float ry = static_cast<float>(projector.origin().y) / project.resolution().height;
		glUniform2f(uniforms_.proj_origin, rx, ry);
	}

	void project_image::draw(const buffer_handle& vertices, const buffer_handle& image_coords, gl::GLsizei count) {
		{
			const auto lock_buffer = bind(vertices);
			glEnableVertexAttribArray(vertex_loc_);
			glVertexAttribPointer(vertex_loc_, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
		}
		{
			const auto lock_buffer = bind(image_coords);
			glEnableVertexAttribArray(image_coord_loc_);
			glVertexAttribPointer(image_coord_loc_, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
		}

		const auto lock_prg = bind(prg_);
		glDrawArrays(GL_TRIANGLES, 0, count);
	}
}
