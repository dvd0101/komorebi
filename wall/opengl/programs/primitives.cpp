#include "primitives.h"

namespace {
	const char* vertex_shader = R"(
#version 100
attribute vec2 vertex;

void main() {
	gl_Position = vec4(vertex, 0.0, 1.0);
})";

	const char* fragment_shader = R"(
#version 100
uniform highp vec3 color;

void main() {
	gl_FragColor = vec4(color, 1.0);
})";
}

using namespace gl;

namespace opengl {
	primitives::primitives()
	    : prg_{assemble(vertex_shader, fragment_shader)},
	      uniforms_{
	          glGetUniformLocation(prg_, "color"),
	      },
	      vertex_loc_{glGetAttribLocation(prg_, "vertex")} {
		const float red[] = {1.f, 0.f, 0.f};
		set_color(red);
	}

	void primitives::set_color(const float color[3]) {
		const auto lock_prg = bind(prg_);
		glUniform3f(uniforms_.color, color[0], color[1], color[2]);
	}

	void primitives::draw(gl::GLenum mode, const buffer_handle& vertices, gl::GLint first, gl::GLsizei count) {
		const auto lock_buffer = bind(vertices);
		glEnableVertexAttribArray(vertex_loc_);
		glVertexAttribPointer(vertex_loc_, 2, GL_FLOAT, GL_FALSE, 0, nullptr);

		const auto lock_prg = bind(prg_);
		glDrawArrays(mode, first, count);
	}
}
