#pragma once
#include "../array_buffer.h"
#include "../program.h"

namespace wall {
	class project;
	class projector;
}

namespace opengl {
	class project_image {
	  public:
		project_image();

		void set_texture_unit(gl::GLenum);
		void set_project_ratio(const wall::project& project, const wall::projector& projector);
		void draw(const buffer_handle&, const buffer_handle&, gl::GLsizei);

	  private:
		program_handle prg_;
		struct {
			gl::GLint image_texture;
			gl::GLint proj_origin;
			gl::GLint area_ratio;
		} uniforms_;
		gl::GLint vertex_loc_;
		gl::GLint image_coord_loc_;
	};
}
