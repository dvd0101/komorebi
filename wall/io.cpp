#include "io.h"
#include <iostream>

io_writer::~io_writer() = default;

void io_writer::log(std::string msg) {
	log_message_ = msg;
}

void io_writer::err(std::string msg) {
	err_message_ = msg;
}

void io_writer::write() {
	if (err_message_) {
		emit_err(*err_message_);
	} else {
		emit_log(log_message_);
	}
	err_message_ = std::nullopt;
	log_message_ = "";
}

void human_io_writer::emit_log(std::string msg) {
	if (msg != "") {
		std::cout << "+OK " << msg << "\n";
	}
}

void human_io_writer::emit_err(std::string msg) {
	std::cout << "+ERR ";
	if (msg != "") {
		std::cout << msg;
	}
	std::cout << "\n";
}

void pipe_io_writer::emit_log(std::string msg) {
	std::cout << "+OK ";
	if (msg != "") {
		std::cout << msg;
	}
	std::cout << std::endl;
}

void pipe_io_writer::emit_err(std::string msg) {
	std::cout << "+ERR ";
	if (msg != "") {
		std::cout << msg;
	}
	std::cout << std::endl;
}
