#pragma once
#include "wall/commands.h"
#include <future>
#include <thread>
#include <vector>

std::pair<std::thread, std::future<void>> start_input_thread();

// pending_commands returns the commands collected by the input thread since
// the previous call.
std::vector<wall::command> pending_commands();
