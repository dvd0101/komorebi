#include "input_thread.h"
#include "wall/parser.h"
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconversion"
#include <asio.hpp>
#define BOOST_COROUTINES_NO_DEPRECATION_WARNING
#include <asio/spawn.hpp>
#pragma GCC diagnostic pop
#include <iostream>
#include <mutex>
#include <system_error>

namespace {
	asio::io_service io;

	std::mutex input_queue_lock;
	std::vector<wall::command> input_queue;

	void log_parse_failure(std::string_view input) {
		std::cerr << "cannot parse: " << input << "\n";
	}

	struct command_handler
	// this handler collects the parsed commands and adds them to the
	// synchronized queue in batch to release the mutex as fast as possible
	{
		void command(const wall::command& cmd) {
			commands.push_back(cmd);
		}

		void parse_failure(std::string_view input) {
			log_parse_failure(input);
		}

		void done() {
			if (!commands.size()) {
				return;
			}
			std::scoped_lock l(input_queue_lock);
			std::move(begin(commands), end(commands), std::back_inserter(input_queue));
			commands.clear();
		};

		std::vector<wall::command> commands;
	};

	void read_input(asio::yield_context yield) {
		using std::begin;
		using std::end;

		// the storage where the input bytes will be kept waiting to be parsed
		std::vector<char> storage(100);

		// write_point is the starting point inside the buffer where the new
		// bytes read from the fd are written.
		// Initially is equal to the begin of the storage, but afterwards is
		// updated to take in account the incomplete commands.
		auto write_point = begin(storage);

		// the buffer mapped over the entire storage, it will be used to derive
		// a sub buffer that take in account the write point
		const auto buffer = asio::buffer(storage);

		command_handler handler;

		asio::posix::stream_descriptor fd(io, ::dup(STDIN_FILENO));
		std::error_code ec;
		while (ec != asio::error::misc_errors::eof) {
			auto sub_range = buffer + std::distance(begin(storage), write_point);
			auto bytes = fd.async_read_some(sub_range, yield[ec]);
			if (bytes > 0) {
				write_point = wall::extract_commands(begin(storage), write_point + bytes, handler);
				if (write_point == end(storage)) {
					// the storage is filled by a single incomplete command
					// that is also invalid (since the size of the storage is
					// sufficient to hold any valid command)
					log_parse_failure({storage.data(), storage.size()});
					write_point = begin(storage);
				}
			}
		}
	}
}

std::pair<std::thread, std::future<void>> start_input_thread() {
	std::promise<void> finished;
	auto f = finished.get_future();
	return std::make_pair(std::thread([finished = std::move(finished)]() mutable {
		                      asio::spawn(io, read_input);
		                      io.run();
		                      finished.set_value();
		                  }),
	                      std::move(f));
}

std::vector<wall::command> pending_commands() {
	std::vector<wall::command> output;

	std::scoped_lock l(input_queue_lock);
	if (!input_queue.empty()) {
		std::swap(output, input_queue);
	}
	return output;
}
