#include "cmdline.h"
#include "configure.h"
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconversion"
#include <boost/program_options.hpp>
#pragma GCC diagnostic pop
#include <cstdlib>
#include <iostream>

namespace po = boost::program_options;

options argparse(int argc, char* argv[]) {
	po::options_description desc("Allowed options");
	desc.add_options()
		("help", "display an help message and exit")
		("version", "output version information and exit")
		("pipe", po::bool_switch(), "program output changes so that this program can be easily driven via a pipe");

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	if (vm.count("help")) {
		std::cout << desc << "\n";
		std::exit(1);
	}

	if (vm.count("version")) {
		std::cout << version << "\n";
		std::exit(0);
	}

	options opt;
	opt.pipe_mode = vm["pipe"].as<bool>();
	return opt;
}
