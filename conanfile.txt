[requires]
Boost.Spirit/1.64.0@bincrafters/stable
Boost.Coroutine/1.64.0@bincrafters/stable
Boost.Program_Options/1.64.0@bincrafters/stable
glbinding/latest@signal9/stable
range-v3/0.3.0@ericniebler/stable

[generators]
cmake
txt
